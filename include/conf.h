
#ifndef _CONF_H_
#define _CONF_H_

#include <map>
#include <string>
using std::map;
using std::string;
#include "common.h"

class Config
{
public:
	/*map<string,int> intconf;
	map<string,string> stringconf;
	map<string,float> floatconf;*/
	map<string, string>conf;

	Config()
	{};
	~Config()
	{};

	inline string getstring (const string& key)
	{
		if (conf.count (key) ) return conf[key];
		return "";
	}
	inline void setstring (const string& key, const string& value)
	{
		conf[key] = value;
	}

	inline int getint (const string& key)
	{
		if (conf.count (key) ) return _stoi (conf[key]);
		return 0;
	}
	inline void setint (const string& key, int value)
	{
		conf[key] = _itos (value);
	}

	inline float getfloat (const string& key)
	{
		if (conf.count (key) ) return _stof (conf[key]);
		return 0;
	}
	inline void setfloat (const string& key, float value)
	{
		conf[key] = _ftos (value);
	}

	bool loadconf (const string& file);
	void parsecmdline (int argc, char**argv);
	void parseenv();
};





#endif
