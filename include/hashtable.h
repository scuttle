#ifndef SCUTTLE_HASHTABLE_H
#define SCUTTLE_HASHTABLE_H

/*
* Simple template hashtable class, using linked lists for resolving
* hash table address colisions.
*
* ! Don't forget to set the hash function before using the table !
* ! Hash function must return values between 0 and tableSize_-1 (or else -> SEGFAULT) !
*/

template <class Object, class Address>
class hashrecord;

template <class Object, class Address>
class hashtable{
    public:
        hashtable();
        ~hashtable();

        void Dim(unsigned tableSize_);
        void Redim(unsigned tableSize_);
        void SetHashFunction(unsigned (*NewHashF)(Address addr_, unsigned tableSize_));

        void Insert(Object data_, Address addr_);
        void Delete(Address addr_);
        Object Search(Address addr_, Object voidObject);
    private:
        unsigned (*HashFunctionCallback)(Address addr_, unsigned tableSize_);
        void Append(Object data_, Address addr_, unsigned position_);

        hashrecord<Object, Address>** table;
        unsigned tableSize;
};

template <class Object, class Address>
class hashrecord{
    public:
        hashrecord(Object data_, Address addr_);

        inline void SetNext(hashrecord<Object, Address>* next_);
        inline hashrecord<Object, Address>* GetNext();
        inline Object GetData();
        inline Address GetAddr();
    private:
        Object data;
        Address addr;
        hashrecord<Object, Address>* next;
};

template <class Object, class Address>
hashtable<Object, Address>::hashtable():
    tableSize(0), table(0), HashFunctionCallback(0){}

template <class Object, class Address>
hashtable<Object, Address>::~hashtable(){
    //If the table exists, clear it and deallocate the hashrecord objects
    if (tableSize != 0){
        for (unsigned i = 0; i < tableSize; ++i){
            //Delete the hashrecord linked list
            while (table[i] != 0){
                hashrecord<Object, Address>* toDelete = table[i];
                table[i] = table[i]->GetNext();
                delete toDelete;
            }
        }
        delete[] table;
    }
}

template <class Object, class Address>
void hashtable<Object, Address>::Dim(unsigned tableSize_){
    Redim(tableSize_);
}

template <class Object, class Address>
void hashtable<Object, Address>::Redim(unsigned tableSize_){
    //Create the new hashtable
    hashrecord<Object, Address>** newTable = new hashrecord<Object, Address>*[tableSize_];
    //Initialize the linked lists
    for (unsigned i = 0; i < tableSize_; ++i)
        newTable[i] = 0;
    unsigned oldTableSize = tableSize;
    tableSize = tableSize_;
    hashrecord<Object, Address>** oldTable = table;
    table = newTable;
    //Rehash the values form the old table to the correct location in the new table
    //and remove the old table simultaneously
    for (unsigned i = 0; i < oldTableSize; ++i){
        while (oldTable[i] != 0){
            unsigned newPosition = HashFunctionCallback(oldTable[i]->GetAddr(), tableSize);
            Append(oldTable[i]->GetData(), oldTable[i]->GetAddr(), newPosition);
            hashrecord<Object, Address>* toDelete = oldTable[i];
            oldTable[i] = oldTable[i]->GetNext();
            delete toDelete;
        }
    }
    //Delete the (now empty) old hash table
    delete[] oldTable;
}

template <class Object, class Address>
void hashtable<Object, Address>::SetHashFunction(unsigned (*NewHashF)(Address, unsigned)){
    HashFunctionCallback = NewHashF;
}

template <class Object, class Address>
void hashtable<Object, Address>::Insert(Object data_, Address addr_){
    Append(data_, addr_, HashFunctionCallback(addr_, tableSize));
}

template <class Object, class Address>
void hashtable<Object, Address>::Delete(Address addr_){
    unsigned pos = HashFunctionCallback(addr_, tableSize);
    hashrecord<Object, Address>* ptr = table[pos];
    hashrecord<Object, Address>* prevptr = 0;
    while (ptr != 0){
        //Delete the object with the given address if found
        if (ptr->GetAddr() == addr_){
            if (prevptr == 0)
                table[pos] = ptr->GetNext();
            else
                prevptr->SetNext(ptr->GetNext());
            delete ptr;
            return;
        }
        ptr = ptr->GetNext();
    }
}

template <class Object, class Address>
Object hashtable<Object, Address>::Search(Address addr_, Object voidObject){
    unsigned pos = HashFunctionCallback(addr_, tableSize);
    hashrecord<Object, Address>* ptr = table[pos];
    while (ptr != 0){
        //Return the object with the given address if found
        if (ptr->GetAddr() == addr_){
            return ptr->GetData();
        }
        ptr = ptr->GetNext();
    }
    //If the object is not found, return the given default object
    return voidObject;
}

template <class Object, class Address>
void hashtable<Object, Address>::Append(Object data_, Address addr_, unsigned position_){
    hashrecord<Object, Address>* newRecord = new hashrecord<Object, Address>(data_, addr_);
    unsigned pos = HashFunctionCallback(addr_, tableSize);
    newRecord->SetNext(table[pos]);
    table[pos] = newRecord;
}

template <class Object, class Address>
hashrecord<Object, Address>::hashrecord(Object data_, Address addr_):
    data(data_), addr(addr_){}

template <class Object, class Address>
void hashrecord<Object, Address>::SetNext(hashrecord<Object, Address>* next_){
    next = next_;
}

template <class Object, class Address>
hashrecord<Object, Address>* hashrecord<Object, Address>::GetNext(){
    return next;
}

template <class Object, class Address>
Object hashrecord<Object, Address>::GetData(){
    return data;
}

template <class Object, class Address>
Address hashrecord<Object, Address>::GetAddr(){
    return addr;
}

#endif
