
#ifndef _PLAYERS_H_
#define _PLAYERS_H_

#include "ship.h"
#include "controller.h"

#include <map>

/*
 * Here we implement slots for network players, attach their controllers,
 * keep exacly one local player, well and similar stuff.
 */

class Player
{
public:
	Controller* control;
	Ship ship;
};

class PlayerSystem
{
public:
	map<int, Player>slot;
	bool update (float time);
	void draw();

	/*TODO,
	 * some routines for adding/deleting of different player types
	 * local player controller instance
	 */
};



#endif

