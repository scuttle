
#ifndef _VIDEO_H_
#define _VIDEO_H_


#include "exa-base.h"

class Video
{
public:
	bool isinit;

	Video()
	{
		isinit = false;
	}
	~Video()
	{
		if (isinit) shutdown();
	}

	void initconf();
	bool init();
	bool shutdown();
	void iconify();
	bool isiconified();
	void nextframe();
};

#endif
