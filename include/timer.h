
#ifndef _TIMER_H_
#define _TIMER_H_


class Timer
{
public:
	float starttime;  //absolute time since clock start
	float frametime;  //the time elapsed since last frame - output

	void start();
	float nextframe (float mintime); //!! call every frame !!
};





#endif
