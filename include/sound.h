
#ifndef _SOUND_H_
#define _SOUND_H_

#ifndef MUSIC_SUBDIR
#define MUSIC_SUBDIR "music/"
#endif

#ifndef SOUNDS_SUBDIR
#define SOUNDS_SUBDIR "sounds/"
#endif

#include "common.h"

#define SoundID uint32_t

class exaStream; //we don't need whole exasound, man!

class Sound
{
public:
	bool isinit;
	float updtime;
	exaStream *musicstream;

	/*
	 * Sound can be configured to be disabled.
	 * All sound calls should first check isinit, if they should
	 * really do anything
	 */

	void initconf();
	bool init();
	void shutdown();

	void update (float time);

	void playsound (SoundID n, complex pos, complex speed, float gain);

	void musicnext();

	//TODO, looping samples should be accesible by their id and
	//can't be adopted by exasound. We must store them somehow, man.
	int startloop (SoundID n, complex pos, complex speed, float gain);
	void setloop (int n, complex pos, complex speed, float gain);
	void stoploop (int n);

	void setlistener (complex pos, complex speed);
};

#endif
