
#ifndef _INPUT_H_
#define _INPUT_H_

#include "conf.h"
#include "common.h"

/*
 * TODO.
 * This system is really fucked up. Redo in the spirit of halflife:
 *
 * bind(key, hitaction, [holdaction,] releaseaction);
 */

#define InputKey uint32_t
#define kExit 0
#define kConsoleSwitch 1
#define k_maxkey 2

class Input
{
public:
	bool keydown[k_maxkey];
	bool keyhit[k_maxkey];
	map<int, InputKey>keybinds;
	map<int, string>cmdbinds;

	//!! mouse keys are mapped as <mouse_key_no>*(-1)

	Input();
	~Input();

	void bind (int ekey, InputKey k);
	void bind (int ekey, const string& command);
	void unbind (int ekey);
	int transl_keyname (const string& kn);

	void initconf();
	void initbind();
	void init();
	void update();
};



#endif
