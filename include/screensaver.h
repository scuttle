
#ifndef _SCREENSAVER_H_
#define _SCREENSAVER_H_

#include "complex.h"
#include <GL/gl.h>

class Screensaver
{
public:
	float time;
	float screenratio;
	GameState mode;
	float ratio;

	float effectparam;

	Screensaver();
	~Screensaver();

	void init();
	void render();
	void update (float time, GameState mode);
	void release();
};

#endif //_SCREENSAVER_H_
