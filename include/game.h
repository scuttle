
#ifndef _GAME_H_
#define _GAME_H_


#include "video.h"
#include "font.h"
#include "timer.h"
#include "input.h"
#include "console.h"
#include "sound.h"
#include "script.h"

#define GameState uint32_t
#define gsStopped 0
#define gsLoading 1
#define gsConnecting 2
#define gsRunnung 3

#include "screensaver.h"

#include <map>
using std::map;


class Game
{
public:
	GameState state;

	Video video;
	Font font;
	Timer timer;
	Input input;
	Console console;
	Config config;
	Sound sound;
	Screensaver screens;

	ScriptEngine script;

	bool done;

	Game()
	{
		state = gsStopped;
		done = false;
	}
	~Game()
	{}

	bool start();
	void stop();
	void initconf();

	bool parseparams (int argc, char* argv[]);

	bool update();
	void render();
};

#ifndef _GAME_CPP_
extern
#endif
	Game game;

int main (int argc, char*argv[]);

#endif
