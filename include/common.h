
#ifndef _COMMON_H_
#define _COMMON_H_

#include <map>
#include <string>
using std::map;
using std::string;

#include <vector>
#define storage std::vector

#undef DFRAND
#undef FRAND
#define DFRAND ((rand()/(float)RAND_MAX)*2-1)
#define FRAND (rand()/(float)RAND_MAX)

#ifndef DATADIR
#define DATADIR "data/"
#endif

#ifndef PIXMAPS_SUBDIR
#define PIXMAPS_SUBDIR "pixmaps/"
#endif

#ifndef DEFAULT_FONT
#define DEFAULT_FONT DATADIR"VeraMono.ttf"
#endif

#define DEFAULT_MAXCLIENTS 32

#include "vector.h"
#include "complex.h"
#include <stdint.h>

inline vector ctov (const complex& c, float depth)
{
	return vector (c.x, c.y, depth);
}

void safefree (void*p);
string _itos (int i);
int _stoi (const string& s);
string _ftos (float f);
float _stof (const string& s);


//Group of endian-specific functions follows

void endian (float*f);
void endian (uint64_t*i);
void endian (uint32_t*i);
void endian (uint16_t*i);
void endian (int64_t*i);
void endian (int32_t*i);
void endian (int16_t*i);
inline void endian (char* c)
{} //just to be complete
void endian (complex*c);
void endian (vector*v);

void randomize();

#ifdef WIN32
int asprintf (char**out, char*fmt, ...);
#endif

#endif
