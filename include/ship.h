
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "controller.h"

#include "common.h"
#include "orientation.h"

class Ship
{
public:
	string nick;
	int score;
	int health;
	float time;

	vector pos, spd, rot;
	orientation ori;
};

#endif

