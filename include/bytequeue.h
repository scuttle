
#ifndef _BYTEQUEUE_H_
#define _BYTEQUEUE_H_

#include <common.h>

#ifndef BYTEQUEUE_DEFAULT_BLOCK_SIZE
#define BYTEQUEUE_DEFAULT_BLOCK_SIZE 1024
//consider changing to 4096
#endif

class ByteQueue
{
	char*d;
	uint32_t s, rs;
public:
	ByteQueue();
	~ByteQueue();

	uint32_t size()
	{
		return s;
	}
	uint32_t append (const char*data, uint32_t size);
	uint32_t append (const ByteQueue& queue);
	char* data()
	{
		return d;
	}
	void clean()
	{
		s = rs = 0;
		if (d) free (d);
		d = NULL;
	}
};




#endif
