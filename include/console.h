
#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include "common.h"
#include "exa-console.h"

class Console
{
public:
	int showstate; //0=hide, 1=small; 2=half; 3=full;
	float showratio;
	float speed;
	float screenratio;
	float showtime;
	int linesonscreen;
	float cursor;

	exaGLConsole console;

	Console()
	{}
	~Console()
	{}

	void initconf();
	bool init();
	void release();

	void echo (const string& str = "")
	{
		console.echo (str.c_str() );
		showtime = 0;
	}
	void printf (const string& format, ...);
	void clear()
	{
		console.clear();
	}
	void clearcmdline()
	{
		console.clearcmdline();
	}
	void show (int s)
	{
		showstate = s % 4;
	}
	void processkey (int k);
	void update (float time);
	void render();
	string getcommand()
	{
		return console.getnextcmdstring (false);
	}
};



#endif
