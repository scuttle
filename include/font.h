
#ifndef _FONT_H_
#define _FONT_H_


#include "exa-font.h"

class Font
{
public:
	exaGLFont drawer;
	bool isinit;

	Font()
	{
		isinit = false;
	}
	~Font()
	{
		release();
	}
	void initconf();
	bool init();
	void release();
};


#include "game.h"


#endif
