

#define _GAME_CPP_
#include "game.h"

static int global_argc;
static char**global_argv;

void Game::initconf()
{
	font.initconf();
	video.initconf();
	input.initconf();
	console.initconf();
	sound.initconf();

	config.setint ("game.mfps", 50);
	config.setint ("console.hs", 1);
	config.setint ("console.ss", 2);
	config.setstring ("configfile", DATADIR"config");
	config.setstring ("autoexec", DATADIR"autoexec");

	//parse the default config file.
	if (!config.loadconf (config.getstring ("configfile") ) )
		script.queuecommand
		("echo \"Loading config file " + config.getstring ("configfile") +
		 " failed. You might want to create it.\"");

	//parse commandline and other args.
	config.parseenv();
	config.parsecmdline (global_argc, global_argv);

	/*
	 * Note: The autoexec can't be used as a config file, 'cause
	 * script execution depends on the running console&video, but
	 * video config would depend on autoexec. Too bad, check
	 * how competitors handle this.
	 */
}

bool Game::start()
{
	if (!video.init() ) return false;
	if (!console.init() ) return false;
	if (!font.init() ) return false;
	if (!sound.init() ) return false;

	randomize();

	screens.init();
	input.init();
	timer.start();
	state = gsStopped;

	script.loadfile (config.getstring ("autoexec") );

	//after the game starts, there should be only the console with a
	//motd message, autoexec loaded. Nothing more (!)

	return true;
}

bool Game::update()
{
	float time;
	int t;
	string s;

	time = timer.nextframe (1.0 / (float) config.getint ("game.mfps") );

	exaUpdate();

	input.update();

	if (input.keyhit[kConsoleSwitch]) {
		t = config.getint ("console.hs");
		if (console.showstate > t)
			console.showstate = t;
		else {
			console.showstate = config.getint ("console.ss");
			console.clearcmdline();
			if (console.showstate < 2) {
				console.showstate = 2;
				config.setint ("console.ss", 2);
			}
		}
	}

	script.update (time);
	console.update (time);
	while ( (s = console.getcommand() ).length() ) {
		script.executecommand (s);
	}

	sound.update (time);

	//DEBUGGING, remove next 3 lines!
	if (exaIsKeyDown (EKEY_F1) ) screens.update (time, gsConnecting);
	else if (exaIsKeyDown (EKEY_F2) ) screens.update (time, gsLoading);
	else screens.update (time, state);

	if (!video.isiconified() ) render();

	return !done;
}

void Game::stop()
{
	sound.shutdown();
	screens.release();
	console.release();
	font.release();
	video.shutdown();
}

bool Game::parseparams (int argc, char*argv[])
{
	return true;
}

void Game::render()
{

	glClear (GL_COLOR_BUFFER_BIT);
	screens.render();
	console.render();
	exaglSwapBuffers();
}

////////////////////////////////////////////

int main (int argc, char*argv[])
{
	global_argc = argc;
	global_argv = argv;

	game.initconf();

	if (!game.parseparams (argc, argv) ) {
		printf ("Error while parsing commandline params.\n");
		return -1;
	}

	if (!game.start() ) {
		printf ("Game initialization error\n");
		return -2;
	}

	while (game.update() );

	game.stop();

	return 0;
}
