#include "input.h"
#include "game.h"
#include "exa-base.h"

static map<string, InputKey> acnames;
class nameslookupInit
{
public:
	nameslookupInit()
	{
		//action names
		acnames["exit"] = kExit;
		acnames["console"] = kConsoleSwitch;
	}
};

static nameslookupInit _nInitializer;

Input::Input()
{
	int i;
	for (i = 0;i < k_maxkey;++i) keydown[i] = keyhit[i] = false;
}

Input::~Input()
{}

void Input::bind (int ekey, InputKey b)
{
	unbind (ekey);
	keybinds[ekey] = b;
}

void Input::bind (int ekey, const string & c)
{
	unbind (ekey);
	if (c.substr (0, 4) == "_KEY_") {
		string s = c.substr (4);
		if (acnames.count (s) )
			keybinds[ekey] = acnames[s];
	} else
		cmdbinds[ekey] = c;
}

void Input::unbind (int ekey)
{
	keybinds.erase (ekey);
	cmdbinds.erase (ekey);
}

void Input::initconf()
{
	//Find something, maybe
	initbind();
}

void Input::initbind()
{
	bind (EKEY_ESCAPE, kExit);
	bind (EKEY_BACKQUOTE, kConsoleSwitch);
}

void Input::init()
{
	exaKeyRepeat (0.4, 0.05);
}

void Input::update()
{
	int i;
	int*k;
	for (i = 0;i < k_maxkey;++i)
		keydown[i] = keyhit[i] = false;
	i = exaGetKeyDowns (&k);
	for (--i;i >= 0;--i)
		if (keybinds.count (k[i]) )
			keydown[keybinds[k[i]]] = true;
	i = exaGetKeyHits (&k);
	for (--i;i >= 0;--i) {
		if (keybinds.count (k[i]) )
			keyhit[keybinds[k[i]]] = true;
		if (cmdbinds.count (k[i]) ) {
			game.script.queuecommand (cmdbinds[k[i]]);
		}
	}
	for (i = 1;i < 4;++i) {
		if (exaIsMouseButtonDown (i) ) {
			if (keybinds.count (-i) )
				keydown[keybinds[-i]] = true;
		}
		if (exaIsMouseButtonHit (i) ) {
			if (keybinds.count (-i) )
				keyhit[keybinds[-i]] = true;
			if (cmdbinds.count (-i) )
				game.script.queuecommand (cmdbinds[-i]);
		}
	}

	if (exaIsMouseWheelUp() ) {
		if (keybinds.count (-4) )
			keyhit[keybinds[-4]] = true;
		if (cmdbinds.count (-4) )
			game.script.queuecommand (cmdbinds[-4]);
	}

	if (exaIsMouseWheelDown() ) {
		if (keybinds.count (-5) )
			keyhit[keybinds[-5]] = true;
		if (cmdbinds.count (-5) )
			game.script.queuecommand (cmdbinds[-5]);
	}

	i = exaGetKeyTypes (&k);
	if (game.console.showratio > 1)
		for (--i;i >= 0;--i)
			game.console.processkey (k[i]);

}
