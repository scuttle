
#include "exa-base.h"
#include "timer.h"
#include <unistd.h>


void Timer::start()
{
	starttime = 0;
	exaGetElapsedTime();
	frametime = 0;
}

float Timer::nextframe (float mintime)
{
	if (! ( (mintime > 0) && (mintime < 0.1) ) ) mintime = 0.1;
	float frametime = exaGetElapsedTime();
	while (frametime < mintime) {
		exaUSleep ( (int) (500000*mintime - frametime) );
		frametime += exaGetElapsedTime();
	}
	return frametime;
}
