
#include "video.h"
#include "game.h"

void Video::initconf()
{
	Config & c = game.config;
	c.setint ("video.x", 800);
	c.setint ("video.y", 600);
	c.setint ("video.bpp", 32);
	c.setint ("video.dbpp", 0);
	c.setstring ("video.windowtitle", "Pumlic");
}

bool Video::init()
{
	Config & c = game.config;
	if (isinit) shutdown();
	exaSetParams (c.getint ("video.x"),
	              c.getint ("video.y"),
	              c.getint ("video.bpp"),
	              c.getint ("video.dbpp"), 0, 0);
	isinit = exaInit();
	if (isinit) exaSetWindowCaption (c.getstring ("video.windowtitle").c_str() );
	return isinit;
}

bool Video::shutdown()
{
	if (!isinit) return true;
	isinit = !exaShutdown();
	return !isinit;
}

void Video::iconify()
{
	if (isinit) exaIconifyWindow();
}

bool Video::isiconified()
{
	if (isinit) return exaIsIconified();
	return false;
}

void Video::nextframe()
{
	exaglSwapBuffers();
}

