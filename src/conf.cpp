
#include "conf.h"
#include "game.h"
#include "common.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

bool Config::loadconf (const string& file)
{
	FILE* f;
	int size, column;
	char*buf;
	char*p, *name, *value;
	bool comment;
	f = fopen (file.c_str(), "r");
	if (!f) return false;

	fseek (f, 0, SEEK_END);
	size = ftell (f);
	fseek (f, 0, SEEK_SET);

	buf = (char*) malloc (size + 1);
	if (!buf) {
		fclose (f);
		return false;
	}
	fread (buf, 1, size, f);
	fclose (f);
	buf[size] = 0; //null terminator protection.

	p = buf;
	name = value = buf;
	column = 0;
	comment = false;
	while (p < buf + size) {
		if (!comment) if ( (*p == ':') && (* (p + 1) == '=') ) {
				*p = 0;
				if (!column) {
					value = p + 2;
					++column;
				}
			}
		if (*p == '\n') {
			if (!comment) {
				*p = 0;
				if (column > 0) setstring (name, value);
			}
			column = 0;
			name = p + 1;
			comment = false;
		}
		if (p == name) if (*p == '#')
				comment = true;
		++p;
	}

	if (column > 0) if (!comment) setstring (name, value);
	return true;
}

/*
 * Commandline options:
 *
 * 1] -s = --set <name> <value>
 * for example,   ... -set video.x 1024
 *
 * 2] -e = --exec <command>
 * this queues a command
 *
 * 3] -c = --config <filename>
 * this parses a config file.
 */

void Config::parsecmdline (int argc, char**argv)
{
	int cmd = 0; // 0/1set/2set/3exec/4conf
	string a;
	char*t = argv[0]; //doesn't have to be initialized, but -Wall bitches :/
	int i;
	for (i = 1;i < argc;++i) switch (cmd) {
		case 1: //set 1
			t = argv[i];
			cmd = 2;
			break;

		case 2: //set 2
			setstring (string (t), string (argv[i]) );
			cmd = 0;
			break;

		case 3: //exec
			game.script.queuecommand (argv[i]);
			cmd = 0;
			break;

		case 4:
			loadconf (argv[i]);
			cmd = 0;
			break;

		case 0:
		default:
			if (argv[i][0] != '-') {
				printf ("unrecognized string: %s\n", argv[i]);
				break;
			}
			t = argv[i] + 1;
			if (!strcmp (t, "s") ) cmd = 1;
			else if (!strcmp (t, "e") ) cmd = 3;
			else if (!strcmp (t, "c") ) cmd = 4;
			else if (!strcmp (t, "-set") ) cmd = 1;
			else if (!strcmp (t, "-exec") ) cmd = 3;
			else if (!strcmp (t, "-config") ) cmd = 4;
			else printf ("unrecognized option: %s\n", t);
			break;
		}
}

/*
 * Format of envoronment variables:
 *
 * SCUTTLE_CMD:
 * script commands, maybe function definitions. They'll be simply executed.
 *
 * SCUTTLE_CONF:
 * Config in this format:
 * <name>=<value>[;<name>=<value>[; ....]]
 *
 * 1] semicolons and equal signs can be backslashed.
 * 2] Any other backslashed char discards the backslash, so use \\
 * 3] Whitespace is NOT ignored.
 */

void Config::parseenv()
{
	char*c = getenv ("SCUTTLE_CONF"), *e = getenv ("SCUTTLE_CMD");

	//as long as the script engine perfectly handles multicommand lines,
	//we can just pass it.
	if (e) game.script.queuecommand (string (e) );

	if (!c) return;
	char *i = c;
	string a, b;
	bool escaped = false;
	int state = 0; //0=reading key; 1==reading value
	while (*i > 0) {
		switch (*i) {
		case '\\':
			if (escaped) {
				escaped = false;
				a.push_back ('\\');
			} else
				escaped = true;
			break;
		case '=':
			if (escaped) {
				escaped = false;
				a.push_back ('=');
			} else if (!state) {
				b = a;
				a = "";
				state = 1;
			} else a.push_back ('=');
			break;
		case ';':
			if (escaped) {
				escaped = false;
				a.push_back (';');
			} else if (state) {
				state = 0;
				setstring (string (b), string (a) );
				a = "";
			} else a.push_back (';');
			break;
		default:
			a.push_back (*i);
			escaped = false;
			break;
		}
		++i;
	}
	if (state == 1) setstring (string (b), string (a) );
}

