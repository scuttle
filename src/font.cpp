
#include "font.h"
#include "common.h"

void Font::initconf()
{
	Config & c = game.config;
	c.setstring ("font.file", DEFAULT_FONT);
	c.setint ("font.resolution", 24);
	//TODO. we could add some functionality which computes font size
	//from available display pixels, so it matches (a little)
}

bool Font::init()
{
	release();
	Config & c = game.config;
	isinit = drawer.loadfromfreetype (c.getstring ("font.file").c_str(),
	                                  c.getint ("font.resolution") );
	if (!isinit) printf ("Font initialization failed. Can't load from %s",
		                     c.getstring ("font.file").c_str() );
	return isinit;
}

void Font::release()
{
	if (isinit) {
		drawer.unload();
		isinit = false;
	}
}



