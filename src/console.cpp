#include "game.h"
#include "console.h"

#include <stdio.h>
#include <stdarg.h>

#define CONSOLE_MESSAGE_TIMEOUT 5

void Console::printf (const string& format, ...)
{
	int n, size = 128;
	char*p, *np;
	va_list ap;

	p = (char*) malloc (size);
	if (!p) return; //prevent segfaults

	//consider using asprintf()!
	while (1) {
		va_start (ap, format);
		n = vsnprintf (p, size, format.c_str(), ap);
		va_end (ap);

		if (n >= 0 && n < size) {
			echo (p);
			free (p);
			return;  //done here
		}
		if (n >= 0) size = n + 1;
		else size *= 2;
		if ( (np = (char*) realloc (p, size) ) == NULL) {
			free (p);
			return;
		}
		p = np;
	}
}

void Console::processkey (int key)
{
	console.EKEYinput (key);
}

void Console::update (float time)
{
	int rs;
	if (showstate > 3) showstate = 3;
	if (showstate < 0) showstate = 0;
	if (showstate < 2) console.unscroll();

	rs = showstate;

	if (rs == 1) {
		if (showtime > CONSOLE_MESSAGE_TIMEOUT) rs = 0;
		else showtime += time;
	} else showtime = 0;

	if (showratio > rs) {
		showratio -= speed * time;
		if (showratio < rs) showratio = rs;
	} else {
		showratio += speed * time;
		if (showratio > rs) showratio = rs;
	}
	cursor -= time * 3;
	if (cursor <= 0) cursor = 1;
}

void Console::initconf()
{
	game.config.setint ("console.scrollback", 512);
	game.config.setint ("console.history", 32);
	game.config.setint ("console.lines", 50);
}

bool Console::init()
{
	showratio = 3.0;
	showstate = 3;
	speed = 4;
	showtime = 0;
	linesonscreen = game.config.getint ("console.lines");
	cursor = 1;
	screenratio = (float) game.config.getint ("video.x") / (float) game.config.getint ("video.y");
	if (! ( (screenratio > 1) && (screenratio < 3) ) ) screenratio = 1;
	console.init (game.config.getint ("console.scrollback"),
	              game.config.getint ("console.history") );
	return true;
}

void Console::release()
{
	console.destroy();
}

void Console::render()
{
	int consolelines;
	float w;
	unsigned char t;
	glMatrixMode (GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho (0, screenratio*linesonscreen, 0, linesonscreen, 1, -1);
	glMatrixMode (GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	if (showratio >= 2) {
		consolelines = (int) (linesonscreen * (0.5 + 0.5 * (showratio - 2) ) );
		glTranslatef (w = 0,
		              linesonscreen * (0.5 * (3 - showratio) ),
		              0);
	} else if (showratio >= 1) {
		consolelines = (int) (linesonscreen * (0.2 + 0.3 * (showratio - 1) ) );
		glTranslatef (w = screenratio * linesonscreen * (0.2 * (2 - showratio) ),
		              linesonscreen * (0.5 + 0.3 * (2 - showratio) ),
		              0);
	} else {
		consolelines = (int) (linesonscreen * 0.2 * showratio);
		glTranslatef (w = screenratio * linesonscreen * 0.2,
		              linesonscreen * (1 - 0.2 * showratio),
		              0);
	}
	w = (screenratio * linesonscreen - (2 * w) );
	if (showratio > 1) {
		consolelines -= 1;
		glTranslatef (0, 1, 0);
	}


	t = 128;
	++consolelines;
	glEnable (GL_BLEND);
	glBlendFunc (GL_DST_COLOR, GL_ZERO);
	glBegin (GL_TRIANGLE_STRIP);
	if (showratio > 2) glColor3ub (t, t, t);
	else if (showratio > 1) {
		t = (unsigned char) (128 * (1 - showratio) );
		glColor3ub (t, t, t);
	} else glColor3ub (255, 255, 255);
	glVertex2f (0, 0);
	glVertex2f (w, 0);
	glColor3ub (128, 128, 128);
	glVertex2f (0, consolelines);
	glVertex2f (w, consolelines);
	glEnd();
	if (showratio > 1) {
		glBegin (GL_TRIANGLE_STRIP);
		glColor3ub (32, 32, 32);
		glVertex2f (0, -1);
		glVertex2f (w, -1);
		glColor3ub (t, t, t);
		glVertex2f (0, 0);
		glVertex2f (w, 0);
		glEnd();
	}
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor3ub (255, 255, 255);
	--consolelines;
	glTranslatef (0.5, 0, 0);
	console.draw (& (game.font.drawer), consolelines, showratio > 1, w - 1, cursor);
	glDisable (GL_BLEND);
	glPopMatrix();
	glMatrixMode (GL_PROJECTION);
	glPopMatrix();
	glMatrixMode (GL_MODELVIEW);
}

