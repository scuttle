
#include "common.h"
#include "exa-socket.h" //because of endian functions
#include <time.h>
#include <stdlib.h>
#include <stdarg.h>

#ifdef WIN32
int asprintf (char** out, char* fmt, ...)
{
	int n, size = 128;
	char*p, *np;
	va_list ap;

	p = (char*) malloc (size);
	if (!p) return -1; //prevent segfaults

	while (size < 65536) {
		va_start (ap, fmt);
		n = vsnprintf (p, size, fmt, ap);
		va_end (ap);

		if (n >= 0 && n < size) {
			*out = p;
			return n;
		}
		if (n >= 0) size = n + 1;
		else size *= 2;
		if ( (np = (char*) realloc (p, size) ) == NULL) {
			free (p);
			return -1;
		}
		p = np;
	}
	return -1;
}
#endif

void safefree (void*p)
{
	if (p) {
		free (p);
		p = NULL;
	}
}


string _itos (int i)
{
	char* a;
	string s;
	asprintf (&a, "%d", i);
	s = a;
	free (a);
	return s;
}

int _stoi (const string& s)
{
	int i;
	sscanf (s.c_str(), "%d", &i);
	return i;
}

string _ftos (float f)
{
	char* a;
	string s;
	asprintf (&a, "%g", f);
	s = a;
	free (a);
	return s;
}

float _stof (const string& s)
{
	float i;
	sscanf (s.c_str(), "%f", &i);
	return i;
}

void endian (float*f)
{
	* ( (int*) f) = htonl (* (int*) f);
}
void endian (uint64_t*i)
{
	register uint32_t t;
	t = htonl (* (uint32_t*) i);
	* (uint32_t*) i = htonl (* (1 + (uint32_t*) i) );
	* (1 + (uint32_t*) i) = t;
}
void endian (int64_t*i)
{
	register uint32_t t;
	t = htonl (* (uint32_t*) i);
	* (uint32_t*) i = htonl (* (1 + (uint32_t*) i) );
	* (1 + (uint32_t*) i) = t;
}


void endian (uint32_t*i)
{
	*i = htonl (*i);
}
void endian (int32_t*i)
{
	*i = htonl (*i);
}
void endian (uint16_t*i)
{
	*i = htons (*i);
}
void endian (int16_t*i)
{
	*i = htons (*i);
}

void endian (complex* c)
{
	endian (& (c->x) );
	endian (& (c->y) );
}
void endian (vector* v)
{
	endian (& (v->x) );
	endian (& (v->y) );
	endian (& (v->z) );
}

void randomize()
{
	srand (time (NULL) );
	rand();
}



