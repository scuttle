

#include "game.h"
#include <GL/gl.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"

static char _str_loading[] = "loading scuttle";
static char _str_connecting[] = "connecting";

#define GenEffectParam (0.4+1.5*(rand()/(float)RAND_MAX))

Screensaver::Screensaver()
{
	time = 0;
	mode = gsStopped;
	ratio = 0;
	effectparam = 1;
}

Screensaver::~Screensaver()
{}

void Screensaver::init()
{
	float a;
	time = 0;
	mode = gsStopped;
	ratio = 0;
	effectparam = GenEffectParam;

	a = game.config.getfloat ("video.y");
	if (a > 0) {
		screenratio = game.config.getfloat ("video.x") / a;
		if (screenratio < 1) screenratio = 1;
	} else screenratio = 1;

	return;
}

void Screensaver::release()
{}

void Screensaver::render()
{
	float a, b, x;
	unsigned char t;
	int i;

	glMatrixMode (GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho (-screenratio, screenratio, -1, 1, -1, 1);
	glMatrixMode (GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glEnable (GL_BLEND);
	glBlendFunc (GL_ONE, GL_ONE);

	switch (mode) {
	case gsStopped:
		glPushMatrix();
		a = 0;
		x = time / 500;
		t = (unsigned char) (ratio * 32);
		for (i = 0;i < 256;++i) {
			a += x;
			a -= truncf (a);
			b = sinf (a * M_PI * 2);
			glRotatef (time*effectparam, 0, 0, 1);
			glPushMatrix();
			glScalef (1, b, 1);
			glBegin (GL_QUADS);
			glColor3ub (t, t >> 2, t >> 3);
			glVertex2f (-2, b);
			glColor3ub (0, 0, 0);
			glVertex2f (-2, b - 0.03);
			glVertex2f (2, b - 0.06);
			glVertex2f (-2, b + 0.03);
			glEnd();
			glPopMatrix();
		};
		glPopMatrix();

		break;

	case gsLoading:
		t = (unsigned char) (ratio * 16);
		glTranslatef (screenratio - 0.3, 0.3, 0);
		glScalef (1.4, 1.4, 1.4);
		x = game.font.drawer.getstrlen (_str_loading);
		glPushMatrix();
		glScalef (0.03, 0.03, 1);
		glRotatef (time*360, 0, 0, 1);
		glTranslatef (-x / 2, 1, 0);
		glColor3ub (0, t*15, 0);
		game.font.drawer.write (_str_loading);
		glPopMatrix();
		for (i = 0;i < 32;++i) {
			glRotatef (time*12, 0, 0, 1);
			glBegin (GL_TRIANGLE_FAN);
			glColor3ub (t, t, t);
			glVertex2f (0, 0.1);
			glColor3ub (0, 0, 0);
			glVertex2f (0, 0.2);
			glVertex2f (-0.03, 0.18);
			glVertex2f (-0.02, 0.04);
			glVertex2f (0.02, 0.04);
			glVertex2f (0.03, 0.18);
			glVertex2f (0, 0.2);
			glEnd();
		}
		break;

	case gsConnecting:
		t = (unsigned char) (ratio * 16);
		glTranslatef (0, -0.4, 0);
		glPushMatrix();
		x = game.font.drawer.getstrlen (_str_connecting);
		glScalef (0.06, 0.06, 1);
		glTranslatef (-x / 2, -0.5, 0);
		glColor3ub (t*8, t*8, t*8);
		game.font.drawer.write (_str_connecting);
		glPopMatrix();
		glPushMatrix();
		glTranslatef (-0.4, 0, 0);
		for (i = 0;i < 16;++i) {
			glRotatef (time*20, 0, 0, 1);
			glBegin (GL_TRIANGLE_FAN);
			glColor3ub (t, t, t);
			glVertex2f (0, 0);
			glColor3ub (0, 0, 0);
			glVertex2f (0.2, 0);
			glVertex2f (0, 0.02);
			glVertex2f (-0.2, 0);
			glVertex2f (0, -0.02);
			glVertex2f (0.2, 0);

			glEnd();
		}
		glPopMatrix();

		glPushMatrix();
		glTranslatef (0.4, 0, 0);
		for (i = 0;i < 16;++i) {
			glRotatef (-time*20, 0, 0, 1);
			glBegin (GL_TRIANGLE_FAN);
			glColor3ub (t, t, t);
			glVertex2f (0, 0);
			glColor3ub (0, 0, 0);
			glVertex2f (0.2, 0);
			glVertex2f (0, 0.02);
			glVertex2f (-0.2, 0);
			glVertex2f (0, -0.02);
			glVertex2f (0.2, 0);
			glEnd();
		}
		glPopMatrix();

		break;
	default:
		break;
	}

	glMatrixMode (GL_PROJECTION);
	glPopMatrix();
	glMatrixMode (GL_MODELVIEW);
	glPopMatrix();

	glDisable (GL_BLEND);
}

void Screensaver::update (float t, GameState m)
{
	time += t;

	if (mode == m) {
		ratio += t * 3;
		if (ratio > 1) ratio = 1;
	} else {
		ratio -= t * 3;
		if (ratio < 0) {
			ratio = 0;
			mode = m;
			time = 0;
			if (mode == gsStopped) {
				effectparam = GenEffectParam;
				game.console.printf ("Effectparam: %.3f",
				                     effectparam);
			}
		}
	}

	switch (mode) {
	case gsStopped:
		glClearColor (0, 0, 0.1*ratio, 0);
		break;
	default:
		glClearColor (0, 0, 0, 0);
	}
}


