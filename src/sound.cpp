
#include "common.h"
#include "sound.h"
#include "game.h"

#undef storage  //because of ogg.h. Select better name.
#include "exa-sound.h"


//directory listing stuff
#ifdef WIN32
# include <io.h>
# include <direct.h>
# include <dirent.h>

/*
 * scandir, the win32 missing function.
 * Desperately taken from ucLibc code.
 * Big thanks for Erik Andersen who most probably originally wrote it!
 */

int scandir (const char *dir, struct dirent ***namelist,
             int (*selector) (const struct dirent *),
             int (*compar) (const void *, const void *) )
{
	DIR *dp = opendir (dir);
	struct dirent *current;
	struct dirent **names = NULL;
	size_t names_size = 0, pos;
	int save;

	if (dp == NULL)
		return -1;

	pos = 0;
	while ( (current = readdir (dp) ) != NULL)
		if (selector == NULL || (*selector) (current) )
		{
			struct dirent *vnew;
			size_t dsize;

			if (pos == names_size) {
				struct dirent **newd;
				if (names_size == 0)
					names_size = 10;
				else
					names_size *= 2;
				newd = (struct dirent **) realloc (names,
				                                   names_size * sizeof
				                                   (struct dirent *) );
				if (newd == NULL)
					break;
				names = newd;
			}

			dsize = &current->d_name[current->d_namlen+1]
			        - (char *) current;
			vnew = (struct dirent *) malloc (dsize);
			if (vnew == NULL)
				break;

			names[pos++] = (struct dirent *) memcpy
			               (vnew, current, dsize);
		}

	if (errno != 0)
	{
		save = errno;
		closedir (dp);
		while (pos > 0)
			free (names[--pos]);
		free (names);
		return -1;
	}

	closedir (dp);

	/* Sort the list if we have a comparison function to sort with.  */
	if (compar != NULL)
		qsort (names, pos, sizeof (struct dirent *), compar);
	*namelist = names;
	return pos;
}


#else
# include <unistd.h>
# include <dirent.h>
# include <sys/types.h>
#endif
#include <stdlib.h>

static int music_shuffle_filter (const struct dirent* de)
{
	int l;
	l = strlen (de->d_name);
	if (l < 5) return 0;
	if (strcasecmp ("ogg", de->d_name + l - 3) ) return 0;
	return 1;
}


static string music_shuffle (const string& dir)
{
	struct dirent** de;
	int n, i;
	string r;
	n = scandir (dir.c_str(), &de, music_shuffle_filter, NULL);
	if (n < 0) return "";
	if (n == 0) {
		free (de);
		return "";
	}
	i = (int) (n * (rand() / (RAND_MAX + 1.0f) ) );
	r = de[i]->d_name;
	while (n--) free (de[n]);
	free (de);
	return r;
}

void Sound::initconf()
{
	game.config.setint ("sound.enabled", 1);
	game.config.setfloat ("sound.master.volume", 0.7);
	game.config.setint ("sound.music.enabled", 1);
	game.config.setfloat ("sound.music.volume", 0.7);
	game.config.setstring ("sound.effects.directory",
	                       DATADIR SOUNDS_SUBDIR);
	game.config.setstring ("sound.music.directory",
	                       DATADIR MUSIC_SUBDIR);
}


bool Sound::init()
{
	shutdown();

	if (!game.config.getint ("sound.enabled") ) {
		isinit = false;
		return true;
	}
	updtime = 0;

	if (!exaSound.init() ) {
		game.console.echo
		("#800Sound initialization error. Disabling sound.");
		isinit = false;
		return true; //we can count it
	}
	isinit = true;

	//now load samples



	musicstream = new exaStream;
	//music starts on first update.
	//so help it:
	updtime = 1;

	return true;
}

void Sound::musicnext()
{
	if (!isinit) return;
	if (!game.config.getint ("sound.music.enabled") ) return;
	string t = game.config.getstring ("sound.music.directory");
	string s = music_shuffle (t);
	if (! (s.length() ) ) {
		game.console.echo ("#544Could not find any music in "
		                   + t + ". Disabling.");
		game.config.setint ("sound.music.enabled", 0);
		return;
	}
	s = t + s;
	musicstream->stop();
	musicstream->load (s.c_str() );
	game.console.printf ("#280*** #666Playing #888%s (%d:%02d)",
	                     s.c_str(),
	                     ( (int) (musicstream->length() ) ) / 60,
	                     ( (int) (musicstream->length() ) ) % 60);
	musicstream->play();
	//musicstream->relative(true);
	musicstream->volume (game.config.getfloat ("sound.music.volume") );
}

void Sound::update (float time)
{
	if (!isinit) return;
	updtime += time;
	if (updtime < 0.1) return;
	updtime = 0;

	exaSound.updateadopted();
	exaSound.globalvolume (game.config.getfloat ("sound.master.volume") );
	if (game.config.getint ("sound.music.enabled") ) {
		musicstream->update();
		if (! (musicstream->isplaying() ) ) musicnext();
		else musicstream->volume (game.config.getfloat ("sound.music.volume") );
	} else musicstream->stop();
}

void Sound::shutdown()
{
	if (!isinit) return;

	if (musicstream) {
		musicstream->stop();
		delete musicstream;
	}
	exaSound.stopadopted();

	//unload all buffers here

	exaSound.shutdown();
	isinit = false;
}

