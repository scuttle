
#include "bytequeue.h"
#include <stdlib.h>

ByteQueue::ByteQueue()
{
	d = NULL;
	s = rs = 0;
}

ByteQueue::~ByteQueue()
{
	if (d) free (d);
}

uint32_t ByteQueue::append (const char*data, uint32_t size)
{
	if (!size) return s;
	uint32_t n = (1 + ( (size + s) / BYTEQUEUE_DEFAULT_BLOCK_SIZE) )
	             * BYTEQUEUE_DEFAULT_BLOCK_SIZE;
	if (n > rs) {
		d = (char*) realloc (d, n);
		rs = n;
	}
	if (!d) {
		s = rs = 0;
	} else {
		memcpy (d + s, data, size);
		s += size;
	}
	return s;
}

uint32_t ByteQueue::append (const ByteQueue& q)
{
	return append (q.d, q.s);
}

