#!/bin/sh
login=$1
[ $login ] || login=$USER
shift

giturl="git+ssh://${login}@repo.or.cz/srv/git/scuttle.git"
git-push "$giturl" $@

